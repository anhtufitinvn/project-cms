import io from './socket.io';

const url = 'https://tuvan.fitin.dev/io';

var $socketIO = {
  init(userID, token) {
    if (!(userID) || !(token)) {
      return null;
    }
    this.disconnect();

    this.userID = userID;
    this.token = token;

    this.socketIO = io(url + `?uid=${this.userID}&token=${this.token}`, {
      transports: ['websocket']
    });

    this.socketIO.on('connect', () =>  {
      // eslint-disable-next-line no-console
      // console.log('socket connected', this.userID + '@' + this.token);
      this.socketIO.emit('join', this.userID + '@' + this.token);
    });
    this.socketIO.on('event', (data) => {
      // eslint-disable-next-line no-console
      console.log('socket event', data);
      this.handle(data);
    });
    this.socketIO.on('on_notification', (data) => {
      // eslint-disable-next-line no-console
      console.log('socket on_notification', data);
      this.handle(data);
    });
    this.socketIO.on('error', (error) => {
      // eslint-disable-next-line no-console
      console.error('socket', error);
    });
    this.socketIO.on('disconnect', (reason) => {
      if (reason === 'io server disconnect') {
        // the disconnection was initiated by the server, you need to reconnect manually
        this.socketIO.connect();
      }
      // else the socket will automatically try to reconnect
      // eslint-disable-next-line no-console
      console.log('socket', reason);
    });
  },
  handle(data) {
    // eslint-disable-next-line no-console
    // console.log('socket handle default', data);
    const dataEvent = data.event || {};
    switch (dataEvent.name) {
      case "notification":
        // if ((dataEvent.payload||{}).message) {
        //   toastr.success((dataEvent.payload||{}).message);
        //   $('.notify-top .red-circle').removeClass('d-none');
        // }
        break;

      default:
        break;
    }
  },
  send(data) {
    // eslint-disable-next-line no-console
    // console.log('socket send update', data);
    this.socketIO.emit('update', data);
  },
  disconnect() {
    if (this.socketIO && typeof this.socketIO.close === 'function'){
      this.socketIO.close();
    }
  }
};
window.$socketIO = $socketIO
// window.addEventListener("load", function() {
//   var token = $('meta[name="access-token"]').attr("content") || '';
//   var uId = $('meta[name="uid"]').attr("content") || '';
//   $socketIO.init(uId, token);
// });
export default $socketIO;