import apiConsultantCMSService from "../services/apiConsultantCMSService";
import handleErrorController from "../services/handleErrorController";
import StyleModel from "../models/style";
import { get } from "lodash";

const state = {
  list: [],
  meta: {},
  loading: false
}

// actions
const actions = {
  async getStylesAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getStylesAPI(params);
      if (response.code === 0) {
        commit("SET_STYLE", StyleModel.list(get(response, 'data.list', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  }
  
}

// getters
const getters = {
  getStyles(state) {
    return state.list;
  },
 
}

// mutations
const mutations = {
  SET_STYLE(state, data) {
    state.list = data;
  },
  SET_META(state, data) {
    state.meta = data;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}