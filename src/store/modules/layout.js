import apiConsultantCMSService from "../services/apiConsultantCMSService";
import handleErrorController from "../services/handleErrorController";
import layoutModel from "../models/layout";
import { get } from "lodash";

const state = {
  list: [],
  meta: {},
  delete: {},
  detail: {},
  new: {},
  loading: false
}

// actions
const actions = {
  async getLayoutsAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getLayoutsAPI(params);
      if (response.code === 0) {
        commit("SET_LAYOUT", layoutModel.list(get(response, 'data.list', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  }, 
  async deleteLayoutAPI({ commit }, layoutId) {
    try {
      const { response } = await apiConsultantCMSService.deleteLayoutAPI(layoutId);
      if (response.code === 0) {
        commit("SET_DELETE", layoutModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async detailLayoutAPI({ commit }, layoutId) {
    try {
      const { response } = await apiConsultantCMSService.detailLayoutAPI(layoutId);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async postLayoutAPI({ commit }, dataForm) {
    try {
      const { response } = await apiConsultantCMSService.postLayoutAPI(dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async putLayoutAPI({ commit }, dataForm) {
    try {
      console.log(dataForm);
      const { response } = await apiConsultantCMSService.putLayoutAPI(dataForm.int_id, dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async cloneLayoutAPI({ commit }, dataForm) {
    try {
      const { response } = await apiConsultantCMSService.cloneLayoutAPI(dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
}

// getters
const getters = {
  getLayouts(state) {
    return state.list;
  },
  getDetail(state) {
    return state.detail;
  },
}

// mutations
const mutations = {
  SET_LAYOUT(state, data) {
    state.list = data;
  },
  SET_META(state, data) {
    state.meta = data;
  },
  SET_DELETE(state, data) {
    state.delete = data;
  },
  SET_DETAIL(state, data) {
    state.detail = data;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}