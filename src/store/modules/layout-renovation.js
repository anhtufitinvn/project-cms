import apiConsultantCMSService from "../services/apiConsultantCMSService";
import handleErrorController from "../services/handleErrorController";
import layoutRenovationModel from "../models/layout-renovation";
import { get } from "lodash";

const state = {
  list: [],
  meta: {},
  delete: {},
  detail: {},
  new: {},
  loading: false
}

// actions
const actions = {
  async getLayoutsRenovationAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getLayoutsRenovationAPI(params);
      if (response.code === 0) {
        commit("SET_LAYOUT", layoutRenovationModel.list(get(response, 'data.list', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  }, 
  async deleteLayoutRenovationAPI({ commit }, layoutId) {
    try {
      const { response } = await apiConsultantCMSService.deleteLayoutRenovationAPI(layoutId);
      if (response.code === 0) {
        commit("SET_DELETE", layoutRenovationModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async detailLayoutRenovationAPI({ commit }, layoutId) {
    try {
      const { response } = await apiConsultantCMSService.detailLayoutRenovationAPI(layoutId);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutRenovationModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async postLayoutRenovationAPI({ commit }, dataForm) {
    try {
      const { response } = await apiConsultantCMSService.postLayoutRenovationAPI(dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutRenovationModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async putLayoutRenovationAPI({ commit }, dataForm) {
    try {
      console.log(dataForm);
      const { response } = await apiConsultantCMSService.putLayoutRenovationAPI(dataForm.int_id, dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutRenovationModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async cloneLayoutRenovationAPI({ commit }, dataForm) {
    try {
      const { response } = await apiConsultantCMSService.cloneLayoutRenovationAPI(dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", layoutRenovationModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
}

// getters
const getters = {
  getLayoutsRenovation(state) {
    return state.list;
  },
  getDetailRenovation(state) {
    return state.detail;
  },
}

// mutations
const mutations = {
  SET_LAYOUT(state, data) {
    state.list = data;
  },
  SET_META(state, data) {
    state.meta = data;
  },
  SET_DELETE(state, data) {
    state.delete = data;
  },
  SET_DETAIL(state, data) {
    state.detail = data;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}