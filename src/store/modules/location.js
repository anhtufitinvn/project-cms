import apiConsultantCMSService from "../services/apiConsultantCMSService";
import handleErrorController from "../services/handleErrorController";
import locationModel from "../models/location";
import { get } from "lodash";

const state = {
  list: [],
  loading: false
}

// actions
const actions = {
  async getLocationsAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getLocationsAPI(params);
      if (response.code === 0) {
        commit("SET_DATA", locationModel.list(get(response, 'data', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
}

// getters
const getters = {
  getLocations(state) {
    return state.list;
  }
}

// mutations
const mutations = {
  SET_DATA(state, data) {
    state.list = data;
  },
  SET_META(state, data) {
    state.meta = data;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}