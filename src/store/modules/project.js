import apiConsultantCMSService from "../services/apiConsultantCMSService";
import handleErrorController from "../services/handleErrorController";
import projectModel from "../models/project";
import { get } from "lodash";

const state = {
  list: [],
  meta: {},
  delete: {},
  detail: {},
  new: {},
  loading: false
}

// actions
const actions = {
  async getProjectsAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getProjectsAPI(params);
      if (response.code === 0) {
        commit("SET_PROJECT", projectModel.list(get(response, 'data.list', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  }, 
  async getProjectsRenovationAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getProjectsRenovationAPI(params);
      if (response.code === 0) {
        commit("SET_PROJECT", projectModel.list(get(response, 'data.list', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  }, 
  async deleteProjectAPI({ commit }, projectId) {
    try {
      const { response } = await apiConsultantCMSService.deleteProjectAPI(projectId);
      if (response.code === 0) {
        commit("SET_DELETE", projectModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async detailProjectAPI({ commit }, projectId) {
    try {
      const { response } = await apiConsultantCMSService.detailProjectAPI(projectId);
      if (response.code === 0) {
        commit("SET_DETAIL", projectModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async postProjectAPI({ commit }, dataForm) {
    try {
      const { response } = await apiConsultantCMSService.postProjectAPI(dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", projectModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async putProjectAPI({ commit }, dataForm) {
    try {
      console.log(dataForm);
      const { response } = await apiConsultantCMSService.putProjectAPI(dataForm.id || dataForm.int_id, dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", projectModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
}

// getters
const getters = {
  getProjects(state) {
    return state.list;
  },
  getDetail(state) {
    return state.detail;
  },
}

// mutations
const mutations = {
  SET_PROJECT(state, data) {
    state.list = data;
  },
  SET_META(state, data) {
    state.meta = data;
  },
  SET_DELETE(state, data) {
    state.delete = data;
  },
  SET_DETAIL(state, data) {
    state.detail = data;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}