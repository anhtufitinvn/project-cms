import apiConsultantCMSService from "../services/apiConsultantCMSService";
import handleErrorController from "../services/handleErrorController";
import roomModel from "../models/room";
import { get } from "lodash";

const state = {
  list: [],
  meta: {},
  delete: {},
  detail: {},
  new: {},
  loading: false
}

// actions
const actions = {
  async getRoomsAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getRoomsAPI(params);
      if (response.code === 0) {
        commit("SET_LAYOUT", roomModel.list(get(response, 'data.list', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  }, 
  async deleteRoomAPI({ commit }, roomId) {
    try {
      const { response } = await apiConsultantCMSService.deleteRoomAPI(roomId);
      if (response.code === 0) {
        commit("SET_DELETE", roomModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async detailRoomAPI({ commit }, roomId) {
    try {
      const { response } = await apiConsultantCMSService.detailRoomAPI(roomId);
      if (response.code === 0) {
        commit("SET_DETAIL", roomModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async postRoomAPI({ commit }, dataForm) {
    try {
      const { response } = await apiConsultantCMSService.postRoomAPI(dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", roomModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  async putRoomAPI({ commit }, dataForm) {
    try {
      console.log(dataForm);
      const { response } = await apiConsultantCMSService.putRoomAPI(dataForm.int_id, dataForm);
      if (response.code === 0) {
        commit("SET_DETAIL", roomModel.detail(get(response, 'data', {})));
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
}

// getters
const getters = {
  getRooms(state) {
    return state.list;
  },
  getDetail(state) {
    return state.detail;
  },
}

// mutations
const mutations = {
  SET_LAYOUT(state, data) {
    state.list = data;
  },
  SET_META(state, data) {
    state.meta = data;
  },
  SET_DELETE(state, data) {
    state.delete = data;
  },
  SET_DETAIL(state, data) {
    state.detail = data;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}