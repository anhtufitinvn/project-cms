import apiConsultantCMSService from "../services/apiConsultantCMSService";
import handleErrorController from "../services/handleErrorController";
import StyleModel from "../models/room-category";
import { get } from "lodash";

const state = {
  list: [],
  meta: {},
  delete: {},
  detail: {},
  new: {},
  loading: false
}

// actions
const actions = {
  async getRoomCategoriesAPI({ commit }, params) {
    try {
      const { response } = await apiConsultantCMSService.getRoomCategoriesAPI(params);
      if (response.code === 0) {
        commit("SET_STYLE", StyleModel.list(get(response, 'data.list', [])));
        commit("SET_META", get(response, 'data.meta', {}))
      } else {
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  }
  
}

// getters
const getters = {
  getRoomCategories(state) {
    return state.list;
  },
 
}

// mutations
const mutations = {
  SET_STYLE(state, data) {
    state.list = data;
  },
  SET_META(state, data) {
    state.meta = data;
  },
  SET_DELETE(state, data) {
    state.delete = data;
  },
  SET_DETAIL(state, data) {
    state.detail = data;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}