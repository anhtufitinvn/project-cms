import sessionHelper from "./sessionHelper"
import { ssn } from "@/config";

const isAuthenticated = () => sessionHelper.get(ssn.token) !== null && sessionHelper.get(ssn.token) !== undefined && sessionHelper.get(ssn.token) !== '';

const getToken = () => {
  const token = sessionHelper.get(ssn.token);
  return token != null ? token : null;
};

const setToken = (token) => {
  return sessionHelper.set(ssn.token, token);
};

const setUID = (uid) => {
  return sessionHelper.set(ssn.uid, uid);
};

const getTokenGuest = () => {
  const token = sessionHelper.get(ssn.guest_token);
  return token != null ? token : null;
};

const setTokenGuest = (token) => {
  return sessionHelper.set(ssn.guest_token, token);
};

export default {
  isAuthenticated,
  getToken,
  setToken,
  getTokenGuest,
  setTokenGuest,
  setUID
};
