import axios from 'axios';
import authService from './authService';
import { 
  API_CMS_LOGIN, 
  API_CMS_LOGOUT, 
  API_CMS_PROJECT, 
  API_CMS_PROJECT_RENOVATION, 
  API_CMS_UPLOAD, API_CMS_LAYOUT , 
  API_CMS_STYLE, API_CMS_ROOM, 
  API_CMS_ROOM_CATEGORY, 
  API_CMS_LAYOUT_RENOVATION, 
  API_CMS_LOCATION, 
  API_CMS_LAYOUT_UPLOAD_LAYOUT,
  API_CMS_LAYOUT_RENOVATION_UPLOAD_CART, 
  API_CMS_LAYOUT_RENOVATION_UPLOAD_PANORAMA,
  API_CMS_LAYOUT_RENOVATION_UPLOAD_LAYOUT,
  API_CMS_LAYOUT_RENOVATION_UPLOAD_PANORAMA_FULL,
  API_CMS_LAYOUT_ROOM,
  API_CMS_LAYOUT_RENOVATION_ROOM,
  API_CMS_LAYOUT_RENOVATION_UPLOAD_GALLERY,
  API_CMS_LAYOUT_RENOVATION_GALLERY,
  API_CMS_LAYOUT_UPLOAD_GALLERY,
  API_CMS_LAYOUT_GALLERY,
  API_CMS_LAYOUT_UPLOAD_CART,
  API_CMS_LAYOUT_UPLOAD_PANORAMA,
  API_CMS_LAYOUT_UPLOAD_PANORAMA_FULL,
  
} from "../../config";

const instance = axios.create();

const apiConsultantCMSService = {
  /**
   *
   * @param {*} method
   * @param {*} url
   * @param {*} [data=null]
   * @param {*} [config={params: {}}]
   * @returns Data
   * @memberof ApiEcomService
   */
  _apiCall: function (method, url, data = null, config = {}) {
    const options = {
      method,
      url,
      data,
      headers: {
        'Content-Type': 'application/json'
      }
    };
    // add params
    if (config.params) {
      options.params = config.params;
    }
    // add auth
    const auth = authService.isAuthenticated();
    if (auth) {
      options.headers = {
        ...options.headers,
        "Authorization": `Bearer ${authService.getToken()}`
      };
    } else {
      options.headers = {
        ...options.headers,
        "Fitin-Guest-Token": `Bearer ${authService.getTokenGuest()}`
      };
    }

    return instance(options).then(res => {
      return {
        response: res.data
      };
    }).catch(err => {
      return Promise.reject(err);
    });
  },

  // Upload
  postUpload: function (dataForm) {
    return this._apiCall("post", API_CMS_UPLOAD, dataForm);
  },

  postLayoutRenovationUploadLayout: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_RENOVATION_UPLOAD_LAYOUT, dataForm);
  },
  postLayoutRenovationUploadGallery: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_RENOVATION_UPLOAD_GALLERY, dataForm);
  },
  deleteLayoutRenovationGallery: function (dataForm) {
    return this._apiCall("delete", API_CMS_LAYOUT_RENOVATION_GALLERY, dataForm);
  },
  postLayoutRenovationUploadCart: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_RENOVATION_UPLOAD_CART, dataForm);
  },
  postLayoutRenovationUploadPanorama: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_RENOVATION_UPLOAD_PANORAMA, dataForm);
  },
  postLayoutRenovationUploadPanoramaFull: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_RENOVATION_UPLOAD_PANORAMA_FULL, dataForm);
  },
  putLayoutRenovationUploadPanoramaFull: function (dataForm) {
    return this._apiCall("put", API_CMS_LAYOUT_RENOVATION_UPLOAD_PANORAMA_FULL, dataForm);
  },

  postLayoutUploadLayout: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_UPLOAD_LAYOUT, dataForm);
  },
  postLayoutUploadGallery: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_UPLOAD_GALLERY, dataForm);
  },
  deleteLayoutGallery: function (dataForm) {
    return this._apiCall("delete", API_CMS_LAYOUT_GALLERY, dataForm);
  },
  postLayoutUploadCart: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_UPLOAD_CART, dataForm);
  },
  postLayoutUploadPanorama: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_UPLOAD_PANORAMA, dataForm);
  },
  postLayoutUploadPanoramaFull: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_UPLOAD_PANORAMA_FULL, dataForm);
  },
  putLayoutUploadPanoramaFull: function (dataForm) {
    return this._apiCall("put", API_CMS_LAYOUT_UPLOAD_PANORAMA_FULL, dataForm);
  },

  // LOGIN
  postLogin: function (dataForm) {
    return this._apiCall("post", API_CMS_LOGIN, dataForm);
  },
  postLogout: function () {
    return this._apiCall("post", API_CMS_LOGOUT);
  },
  // PROJECT
  getProjectsAPI: function (params) {
    return this._apiCall("get", API_CMS_PROJECT, null, params);
  },
  getProjectsRenovationAPI: function (params) {
    return this._apiCall("get", API_CMS_PROJECT_RENOVATION, null, params);
  },
  detailProjectAPI: function (projectId) {
    return this._apiCall("get", API_CMS_PROJECT + '/' + projectId);
  },
  deleteProjectAPI: function (projectId) {
    return this._apiCall("delete", API_CMS_PROJECT + '/' + projectId);
  },
  postProjectAPI: function (dataForm) {
    return this._apiCall("post", API_CMS_PROJECT, dataForm);
  },
  putProjectAPI: function (projectId, dataForm) {
    return this._apiCall("put", API_CMS_PROJECT + '/' + projectId, dataForm);
  },
  // LAYOUT
  getLayoutsAPI: function (params) {
    return this._apiCall("get", API_CMS_LAYOUT, null, params);
  },
  detailLayoutAPI: function (layoutId) {
    return this._apiCall("get", API_CMS_LAYOUT + '/' + layoutId);
  },
  deleteLayoutAPI: function (layoutId) {
    return this._apiCall("delete", API_CMS_LAYOUT + '/' + layoutId);
  },
  postLayoutAPI: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT, dataForm);
  },
  putLayoutAPI: function (layoutId, dataForm) {
    return this._apiCall("put", API_CMS_LAYOUT + '/' + layoutId, dataForm);
  },
  cloneLayoutAPI: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT + '/duplicate' , dataForm);
  },
  // LAYOUT_ROOM
  createLayoutRoomAPI: function (dataForm) {
    return this._apiCall('post', API_CMS_LAYOUT_ROOM, dataForm);
  },
  deleteLayoutRoomAPI: function (dataForm) {
    return this._apiCall('delete', API_CMS_LAYOUT_ROOM, dataForm);
  },
  // LAYOUT_RENOVATION_ROOM
  createLayoutRenovationRoomAPI: function (dataForm) {
    return this._apiCall('post', API_CMS_LAYOUT_RENOVATION_ROOM, dataForm);
  },
  deleteLayoutRenovationRoomAPI: function (dataForm) {
    return this._apiCall('delete', API_CMS_LAYOUT_RENOVATION_ROOM, dataForm);
  },

  // ROOM
  getRoomsAPI: function (params) {
    return this._apiCall("get", API_CMS_ROOM, null, params);
  },
  detailRoomAPI: function (roomId) {
    return this._apiCall("get", API_CMS_ROOM + '/' + roomId);
  },
  deleteRoomAPI: function (roomId) {
    return this._apiCall("delete", API_CMS_ROOM + '/' + roomId);
  },
  postRoomAPI: function (dataForm) {
    return this._apiCall("post", API_CMS_ROOM, dataForm);
  },
  putRoomAPI: function (roomId, dataForm) {
    return this._apiCall("put", API_CMS_ROOM + '/' + roomId, dataForm);
  },

  // LAYOUT RENOVATION
  getLayoutsRenovationAPI: function (params) {
    return this._apiCall("get", API_CMS_LAYOUT_RENOVATION, null, params);
  },
  detailLayoutRenovationAPI: function (layoutId) {
    return this._apiCall("get", API_CMS_LAYOUT_RENOVATION + '/' + layoutId);
  },
  deleteLayoutRenovationAPI: function (layoutId) {
    return this._apiCall("delete", API_CMS_LAYOUT_RENOVATION + '/' + layoutId);
  },
  postLayoutRenovationAPI: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_RENOVATION, dataForm);
  },
  putLayoutRenovationAPI: function (layoutId, dataForm) {
    return this._apiCall("put", API_CMS_LAYOUT_RENOVATION + '/' + layoutId, dataForm);
  },
  cloneLayoutRenovationAPI: function (dataForm) {
    return this._apiCall("post", API_CMS_LAYOUT_RENOVATION + '/duplicate' , dataForm);
  },

  //STYLE
  getStylesAPI: function (params) {
    return this._apiCall("get", API_CMS_STYLE, null, params);
  },

  getRoomCategoriesAPI: function (params) {
    return this._apiCall("get", API_CMS_ROOM_CATEGORY, null, params);
  },

  // Locations
  getLocationsAPI: function (params) {
    return this._apiCall("get", API_CMS_LOCATION, null, params);
  },
}

export default apiConsultantCMSService;
