import axios from 'axios';
import authService from './authService';
import { API_PROJECT, API_LAYOUT, API_BUILD_QUOTATION_LAYOUT, API_BUILD_QUOTATION_LAYOUT_RENOVATION, API_BUILD_QUOTATION_LAYOUT_RENOVATION_ALL } from "../../config";

const instance = axios.create();
/**
 *
 * @param {*} method
 * @param {*} url
 * @param {*} [data=null]
 * @param {*} [config={params: {}}]
 * @returns Data
 * @memberof ApiEcomService
 */
function _apiCall (method, url, data, config = {}) {
  const options = {
    method,
    url,
    data: data || null,
    headers: {
      'Content-Type': 'application/json'
    }
  };
  // add params
  if (config.params) {
    options.params = config.params;
  }
  // add auth
  const auth = authService.isAuthenticated();
  if (auth) {
    options.headers = {
      ...options.headers,
      "Authorization": `${authService.getToken()}`
    };
  } else {
    options.headers = {
      ...options.headers,
      "Fitin-Guest-Token": `${authService.getTokenGuest()}`
    };
  }

  return instance(options).then(res => {
    return {
      response: res.data
    };
  });
}

const apiConsultantService = {

  // PROJECT
  getProjectsAPI: function () {
    return _apiCall("get", API_PROJECT);
  },
  // LAYOUT
  getLayoutsByProjectAPI: function (projectId) {
    return _apiCall("get", API_LAYOUT + projectId);
  },

  getBuildQuotationLayout: function (layoutId) {
    return _apiCall('get', API_BUILD_QUOTATION_LAYOUT + layoutId);
  },

  getBuildQuotationLayoutRenovation: function (layoutId) {
    return _apiCall('get', API_BUILD_QUOTATION_LAYOUT_RENOVATION + layoutId);
  },


  getBuildQuotationRenovationAll: function () {
    return _apiCall('get', API_BUILD_QUOTATION_LAYOUT_RENOVATION_ALL);
  },
};

export default apiConsultantService;
