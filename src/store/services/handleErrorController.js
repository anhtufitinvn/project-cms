import App from "@/main";
import router from "@/router/index";
export default function (error) {
  // eslint-disable-next-line no-console
  console.log('handleErrorController', error, App);
  App.$bvToast.toast(`${error.message}`, {
    title: 'Error!',
    autoHideDelay: 3000,
    variant: 'danger',
    appendToast: true
  });
  if (error.code === -401) {
    router.push({name: 'Login'});
  }
}