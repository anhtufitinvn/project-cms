import Cookie from 'js-cookie'

export default {
  get: function(name) {
    return Cookie.get(name);
  },

  set: function(name, data) {
    return Cookie.set(name, data);
  },

  remove: function(name) {
    return Cookie.remove(name);
  }
};