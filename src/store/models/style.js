
const StyleModel = {
  detail: function (dataRaw = {}) {
    
    let data = {
      "style_name" :  dataRaw.name || null,
      "style_id":  dataRaw._id || null,
      "_id":  dataRaw._id || null,
      "name" :  dataRaw.name || null,
      "image" :  dataRaw.image || null,
      "image_url" :  dataRaw.image_url || null,
      "attributes": dataRaw.attributes || {},
      "created_at": dataRaw.created_at || "",
      "updated_at": dataRaw.updated_at || "",
      "status" : dataRaw.status, 
      "slug": dataRaw.slug || "",
    }
    return data;
  },

  list: function (dataRawList = []) {
    return dataRawList.map(dataRaw => this.detail(dataRaw));
  }
}
export default StyleModel;
