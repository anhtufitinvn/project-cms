
const LocationModel = {
  detail: function (dataRaw = {}) {
    let data = {
      "city_id" :  dataRaw.city_id || null,
      "city_name" :  dataRaw.city_name || null,
      "districts" :  dataRaw.districts || []
    }
    return data;
  },
  
  list: function(dataRawList = []) {
    return dataRawList.map(dataRaw => this.detail(dataRaw));
  },
}
export default LocationModel;
