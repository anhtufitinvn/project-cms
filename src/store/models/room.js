import { get } from "lodash";
const RoomModel = {
  detail: function(dataRaw = {}) {
    return {
      int_id: dataRaw.int_id || 0,
      room_id: dataRaw.room_id || null,
      layout_name: dataRaw.layout_name || "",
      layout_id: dataRaw.layout_id || null,
      layout_int_id: dataRaw.layout_int_id || null,
      room_name: dataRaw.room_name || "",
      room_category_name: dataRaw.room_category_name || "",
      room_category_key: dataRaw.room_category_key || "",
      slug: dataRaw.slug || "",
      image_path: dataRaw.image_path || "",
      image_url: dataRaw.image_url || "",
      area: dataRaw.area || 0,
      //image_floor_plan_path: dataRaw.image_floor_plan_path || "",
      //room_image_floor_plan_url: dataRaw.room_image_floor_plan_url || "",
      room_json: dataRaw.room_json || [],
      status : dataRaw.status || null,
      desc: dataRaw.desc || null,
      metadata: {
        title: get(dataRaw, 'metadata.title', ""),
        intro: get(dataRaw, 'metadata.intro', null),
       
        price: {
          estimated: get(dataRaw, 'metadata.price.estimated', 0),
          min: get(dataRaw, 'metadata.price.min', 0),
          max: get(dataRaw, 'metadata.price.max', 0),
        }
       
      },
      attributes: dataRaw.attributes || [],
      
      extensions: {
        cart_json: get(dataRaw, 'extensions.cart_json', []),
        services_fee: get(dataRaw, 'extensions.services_fee', []),
        panorama: {
          enable: get(dataRaw, 'extensions.panorama.enable', false),
          url: get(dataRaw, 'extensions.panorama.url', null),
        },
      },
      publishing: {
        published_by: get(dataRaw, 'publishing.published_by', ""),
        published_at: get(dataRaw, 'publishing.published_at', null),
      },
      gallery:  get(dataRaw, 'gallery', []),
      updated_at: dataRaw.updated_at || "",
      created_at: dataRaw.created_at || "",
    };
  },

  list: function(dataRawList = []) {
    return dataRawList.map(dataRaw => this.detail(dataRaw));
  },
};
export default RoomModel;
