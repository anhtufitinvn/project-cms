
const RoomCategoryModel = {
  detail: function (dataRaw = {}) {
    
    let data = {
      "name" :  dataRaw.name || null,
      "type" :  dataRaw.type || null,
      "image" :  dataRaw.image || null,
      "image_url" :  dataRaw.image_url || null,
      "_id":  dataRaw._id || null,
    }
    return data;
  },

  list: function (dataRawList = []) {
    return dataRawList.map(dataRaw => this.detail(dataRaw));
  }
}
export default RoomCategoryModel;
