import { get } from "lodash";
const LayoutModel = {
  detail: function(dataRaw = {}) {
    return {
      int_id: dataRaw.int_id || 0,
      parent_int_id: dataRaw.parent_int_id || 0,
      layout_key_name: dataRaw.layout_key_name || null,
      project_name: dataRaw.project_name || "",
      project_id: dataRaw.project_id || null,
      project_int_id: dataRaw.project_int_id || null,
      layout_name: dataRaw.layout_name || "",
      slug: dataRaw.slug || "",
      image_path: dataRaw.image_path || "",
      layout_image_url: dataRaw.layout_image_url || "",
      area: dataRaw.area || 0,
      image_floor_plan_path: dataRaw.image_floor_plan_path || "",
      layout_image_floor_plan_url: dataRaw.layout_image_floor_plan_url || "",
      //layout_json: dataRaw.layout_json || [],
      status : dataRaw.status || null,
      is_template : dataRaw.is_template,
      desc: dataRaw.desc || null,
      metadata: {
        title: get(dataRaw, 'metadata.title', ""),
        intro: get(dataRaw, 'metadata.intro', null),
        style: {
          key: get(dataRaw, 'metadata.style.key', ''),
          name: get(dataRaw, 'metadata.style.name', ''),
        },
        user_info : get(dataRaw, 'metadata.user_info' , {}),
        price: {
          estimated: get(dataRaw, 'metadata.price.estimated', 0),
          min: get(dataRaw, 'metadata.price.min', 0),
          max: get(dataRaw, 'metadata.price.max', 0),
        },
        rooms: get(dataRaw, 'metadata.rooms', []),
        bedroom_num : get(dataRaw, 'metadata.bedroom_num', 0),
        creator: {
          int_id : get(dataRaw, 'metadata.creator.int_id', null),
          email : get(dataRaw, 'metadata.creator.email', null)
        },
        address : {
          block: get(dataRaw, 'metadata.address.block', ''),
          apartment_code : get(dataRaw, 'metadata.address.apartment_code', '')
        },
        approved : get(dataRaw, 'metadata.approved', false)
      },
      attributes: dataRaw.attributes || [],
      // location: {
      //   // city_id: get(dataRaw, 'location.city_id', 0),
      //   // district_id: get(dataRaw, 'location.district_id', 0),
      //   // lat: get(dataRaw, 'location.lat', 0),
      //   // long: get(dataRaw, 'location.long', 0),
      //   // city_name: get(dataRaw, 'location.city_name', ''),
      //   // district_name: get(dataRaw, 'location.district_name', ''),
      //   // address: get(dataRaw, 'location.address', ''),
      //   block: get(dataRaw, 'location.block', ''),
      //   apartment_code: get(dataRaw, 'location.apartment_code', ''),
      //   // disctrict_name: get(dataRaw, 'location.disctrict_name', ''),
      // },
      extensions: {
        cart_json: get(dataRaw, 'extensions.cart_json', []),
        services_fee: get(dataRaw, 'extensions.services_fee', []),
        panorama: {
          enable: get(dataRaw, 'extensions.panorama.enable', false),
          url: get(dataRaw, 'extensions.panorama.url', null),
        },
      },
      publishing: {
        published_by: get(dataRaw, 'publishing.published_by', ""),
        published_at: get(dataRaw, 'publishing.published_at', null),
      },
      gallery:  get(dataRaw, 'gallery', []),
      updated_at: dataRaw.updated_at || "",
      created_at: dataRaw.created_at || "",
      quotation_url: dataRaw.quotation_url || null,
      cart_url: dataRaw.cart_url || null,
    };
  },

  list: function(dataRawList = []) {
    return dataRawList.map(dataRaw => this.detail(dataRaw));
  },
};
export default LayoutModel;
