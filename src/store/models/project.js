import { get } from "lodash";
const ProjectModel = {
  detail: function (dataRaw = {}) {
    
    let data = {
      "attributes": dataRaw.attributes || {},
      "created_at": dataRaw.created_at || "",
      "updated_at": dataRaw.updated_at || "",
      "desc": dataRaw.desc || "",
      "image_path": dataRaw.image_path || null,
      "project_image_url": dataRaw.project_image_url || null,
      "location": {
        "city_id": get(dataRaw, 'location.city_id', 0),
        "district_id": get(dataRaw, 'location.district_id', 0),
        "lat": get(dataRaw, 'location.lat', 0),
        "long": get(dataRaw, 'location.long', 0),
        "city_name": get(dataRaw, 'location.city_name', ''),
        "district_name": get(dataRaw, 'location.district_name', ''),
        "address": get(dataRaw, 'location.address', ''),
        "block": get(dataRaw, 'location.block', ''),
        "apartment_code": get(dataRaw, 'location.apartment_code', ''),
        "disctrict_name": get(dataRaw, 'location.disctrict_name', ''),
      },
      "metadata": dataRaw.metadata || { 
        "title" : get(dataRaw, 'metadata.title', ''),
        "title_desc" :  get(dataRaw, 'metadata.title_desc', '')
      },
      "project_name": dataRaw.project_name || "",
      "status" : dataRaw.status, 
      "is_renovation" : dataRaw.is_renovation,
      "slug": dataRaw.slug || "",
      "landing_url" : dataRaw.landing_url || '',
      "sort" : dataRaw.sort || 0,
      "_id":  dataRaw._id || null,
      "int_id" :  dataRaw.int_id || null
    }
    return data;
  },

  list: function (dataRawList = []) {
    return dataRawList.map(dataRaw => this.detail(dataRaw));
  }
}
export default ProjectModel;
