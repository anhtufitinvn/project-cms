import Vue from 'vue'
import Vuex from 'vuex'

import project from "./modules/project"
import layout from "./modules/layout"
import layoutRenovation from "./modules/layout-renovation"
import style from "./modules/style"
import room from "./modules/room"
import roomCategory from "./modules/room-category"
import location from "./modules/location"

import apiConsultantCMSService from "./services/apiConsultantCMSService";
import authService from "./services/authService";
import handleErrorController from "./services/handleErrorController";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    TOKEN: '',
    auth: {}
  },
  mutations: {
    SET_AUTH(state, data) {
      state.auth = data;
      state.TOKEN = data.access_token;
      authService.setToken(data.access_token);
      authService.setUID(data.id || 0);
    },
    RESET_AUTH(state) {
      state.auth = {};
      state.TOKEN = '',
      authService.setToken(null);
      authService.setUID(0);
    }
  },
  actions: {
    async postLogin({ commit }, dataForm) {
      try {
        const { response } = await apiConsultantCMSService.postLogin({
          email: dataForm.email,
          password: dataForm.password,
        });
        if (response.code === 0) {
          commit("SET_AUTH", response.data);
        } else {
          handleErrorController(response);
        }
        return response;
      } catch (error) {
        handleErrorController(error);
      }
    },
    async postLogout({ commit }) {
      try {
        commit("RESET_AUTH");
        return {code: 0, message: 'success'};
        // const { response } = await apiConsultantCMSService.postLogout({
        // });
        // if (response.code === 0) {
        //   commit("SET_AUTH", '');
        // } else {
        //   handleErrorController(response);
        // }
        // return response;
      } catch (error) {
        handleErrorController(error);
      }
    },
  },
  modules: {
    project,
    layout,
    style,
    room,
    roomCategory,
    layoutRenovation,
    location
  }
})
