import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import store from './store'
import router from './router'
import './registerServiceWorker'

import "./scss/index.scss";

import $socketIO from "./plugins/socket.io/socketIO";
$socketIO.init(1, '1a17257b-63df-4a84-b077-630453451ede');

Vue.config.productionTip = false

export default new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
