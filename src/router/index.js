import Vue from 'vue'
import VueRouter from 'vue-router'
import authService from "../store/services/authService"

import { URLHome, URLPanoramaComment, URLProject, URLProjectRenovation, URLLogin, URLAdmin, URLLayout, URLRoom, URLLayoutRenovation, URLDashboard } from "../config";

Vue.use(VueRouter)

const routes = [
  {
    path: URLHome,
    redirect: URLAdmin,
  },
  // PanoramaComment
  {
    path: URLPanoramaComment,
    name: "PanoramaComment",
    component: () =>
      import(
        /* webpackChunkName: "panoramaComment" */ "../views/panoramaComment/index.vue"
      ),
  },
  // Login
  {
    path: URLLogin,
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/login/index.vue"),
  },
  // Admin Group
  {
    path: URLAdmin,
    name: 'Admin',
    component: () => import(/* webpackChunkName: "layoutDefault" */ '../layouts/default.vue'),
    beforeEnter: (to, from, next) => {
      // ...
      if (authService.isAuthenticated()) {
        next();
      } else {
        next({ name: "Login", query: { redirect: to.fullPath } });
      }
    },
    children: [
      {
        path: URLAdmin,
        redirect: URLDashboard
      },
      // Dashboard
      {
        path: `${URLDashboard}`,
        name: 'Dashboard',
        component: () => import(/* webpackChunkName: "dashboard" */ '../views/home/index.vue')
      },
      // Room
      {
        path: `${URLRoom}/:id`,
        name: "RoomDetail",
        component: () =>
          import(
            /* webpackChunkName: "roomDetail" */ "../views/room/detail.vue"
          ),
      },
      {
        path: URLRoom,
        name: "RoomList",
        component: () =>
          import(/* webpackChunkName: "layoutList" */ "../views/room/list.vue"),
      },
      // Layout-renovation
      {
        path: `${URLLayoutRenovation}/:id`,
        name: "LayoutRenovationDetail",
        component: () =>
          import(
            /* webpackChunkName: "layoutDetail" */ "../views/layout-renovation/detail.vue"
          ),
      },
      {
        path: URLLayoutRenovation,
        name: "LayoutRenovationList",
        component: () =>
          import(
            /* webpackChunkName: "layoutList" */ "../views/layout-renovation/list.vue"
          ),
      },
      // Layout
      {
        path: `${URLLayout}/:id`,
        name: "LayoutDetail",
        component: () =>
          import(
            /* webpackChunkName: "layoutDetail" */ "../views/layout/detail.vue"
          ),
      },
      {
        path: URLLayout,
        name: "LayoutList",
        component: () =>
          import(
            /* webpackChunkName: "layoutList" */ "../views/layout/list.vue"
          ),
      },
      // Project
      {
        path: `${URLProject}/:id`,
        name: "ProjectDetail",
        component: () =>
          import(
            /* webpackChunkName: "projectDetail" */ "../views/project/detail.vue"
          ),
      },
      {
        path: URLProject,
        name: "ProjectList",
        component: () =>
          import(
            /* webpackChunkName: "projectList" */ "../views/project/list.vue"
          ),
      },
      // Project Renovation
      {
        path: URLProjectRenovation,
        name: "ProjectRenovationList",
        component: () =>
          import(
            /* webpackChunkName: "projectList" */ "../views/project-renovation/list.vue"
          ),
      },
      {
        path: `${URLProjectRenovation}/:id`,
        name: "ProjectRenovationDetail",
        component: () =>
          import(
            /* webpackChunkName: "projectDetail" */ "../views/project-renovation/detail.vue"
          ),
      },
    ],
  },
  {
    path: '/500',
    name: "500",
    component: () =>
      import(
        /* webpackChunkName: "500" */ "../views/error/500.vue"
      ),
  },
  {
    path: '/403',
    name: "403",
    component: () =>
      import(
        /* webpackChunkName: "403" */ "../views/error/403.vue"
      ),
  },
  {
    path: '/404',
    name: "404",
    component: () =>
      import(
        /* webpackChunkName: "404" */ "../views/error/404.vue"
      ),
  },
  {
    path: '*',
    name: "404",
    component: () =>
      import(
        /* webpackChunkName: "404" */ "../views/error/404.vue"
      ),
  },
];

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
