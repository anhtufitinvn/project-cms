export const API_BASE = process.env.VUE_APP_API_BASE || 'https://api.fitin.dev';
export const API_PRODUCT_BY_OBJECTKEY = API_BASE + '/api/frontend/v1/products/by-object-key-names?object_key=';

export const ssn = {
  token: '_fid',
  uid: '_uid',
  guest_token: '_gid',
}

// URL
const PublicPath = process.env.VUE_APP_PUBLIC_PATH || '/';
export const URLHome = PublicPath;
export const URLLogin = URLHome + 'login';
export const URLPanoramaComment = URLHome + 'panorama-comment';
export const URLAdmin = URLHome + 'cms';
export const URLDashboard = URLAdmin + '/dashboard';
export const URLProject = URLAdmin + '/project';
export const URLProjectRenovation = URLAdmin + '/project-renovation';
export const URLLayout = `${URLProject}/layout`;
export const URLLayoutRenovation = `${URLProjectRenovation}/layout-renovation`;
export const URLRoom = `${URLProject}/:project_id/layout/:layout_id/room`;

// API
export const API_BASE_CONSULTANT = process.env.VUE_APP_API_BASE_CONSULTANT || 'https://tuvan.fitin.dev';
//export const API_BASE_CONSULTANT = 'http://localhost:4915';
export const API_PROJECT = API_BASE_CONSULTANT + '/api/v1/projects';
export const API_LAYOUT = API_BASE_CONSULTANT + '/api/v1/projects?project_id=';
export const API_BUILD_QUOTATION_LAYOUT = API_BASE_CONSULTANT + '/api/v2/quotation/template/build/layout/';
export const API_BUILD_QUOTATION_LAYOUT_RENOVATION = API_BASE_CONSULTANT + '/api/v2/quotation/renovation/build/layout/';
export const API_BUILD_QUOTATION_LAYOUT_RENOVATION_ALL = API_BASE_CONSULTANT + '/api/v2/quotation/renovation/build/all';

// API CMS
export const API_CMS_BASE_CONSULTANT = process.env.VUE_APP_API_CMS_BASE_CONSULTANT || 'https://consultant-cms-api.fitin.dev';

//export const API_CMS_LOGIN = API_CMS_BASE_CONSULTANT + '/api/cms/v1/auth/login';
//export const API_CMS_PROJECT = API_CMS_BASE_CONSULTANT + '/api/cms/v1/projects';
//export const API_CMS_UPLOAD = API_CMS_BASE_CONSULTANT + '/api/cms/v1/upload/image';
//export const API_CMS_LAYOUT = API_CMS_BASE_CONSULTANT + '/api/cms/v1/layouts';

export const API_CMS_LOGIN = API_BASE_CONSULTANT + '/cms/v1/auth/login';
export const API_CMS_LOGOUT = API_BASE_CONSULTANT + '/cms/v1/auth/logout';

export const API_CMS_PROJECT = API_BASE_CONSULTANT + '/cms/v1/projects';
export const API_CMS_PROJECT_RENOVATION = API_BASE_CONSULTANT + '/cms/v1/renovation/projects';

export const API_CMS_UPLOAD = API_BASE_CONSULTANT + '/cms/v1/upload/image';

export const API_CMS_LAYOUT = API_BASE_CONSULTANT + '/cms/v1/layouts';
export const API_CMS_LAYOUT_ROOM = API_BASE_CONSULTANT + '/cms/v1/layouts/rooms';
export const API_CMS_LAYOUT_UPLOAD_CART = API_BASE_CONSULTANT + '/cms/v1/layouts/upload/cart';
export const API_CMS_LAYOUT_UPLOAD_LAYOUT = API_BASE_CONSULTANT + '/cms/v1/layouts/upload/json';
export const API_CMS_LAYOUT_UPLOAD_PANORAMA = API_BASE_CONSULTANT + '/cms/v1/layouts/upload/panorama';
export const API_CMS_LAYOUT_UPLOAD_PANORAMA_FULL = API_BASE_CONSULTANT + '/api/v1/import/layout/panorama';
export const API_CMS_LAYOUT_UPLOAD_GALLERY = API_BASE_CONSULTANT + '/cms/v1/layouts/upload/gallery';
export const API_CMS_LAYOUT_GALLERY = API_BASE_CONSULTANT + '/cms/v1/layouts/gallery';

export const API_CMS_LAYOUT_RENOVATION = API_BASE_CONSULTANT + '/cms/v1/layouts-renovation';
export const API_CMS_LAYOUT_RENOVATION_ROOM = API_BASE_CONSULTANT + '/cms/v1/layouts-renovation/rooms';
export const API_CMS_LAYOUT_RENOVATION_UPLOAD_CART = API_BASE_CONSULTANT + '/cms/v1/layouts-renovation/upload/cart';
export const API_CMS_LAYOUT_RENOVATION_UPLOAD_LAYOUT = API_BASE_CONSULTANT + '/cms/v1/layouts-renovation/upload/json';
export const API_CMS_LAYOUT_RENOVATION_UPLOAD_PANORAMA = API_BASE_CONSULTANT + '/cms/v1/layouts-renovation/upload/panorama';
export const API_CMS_LAYOUT_RENOVATION_UPLOAD_PANORAMA_FULL = API_BASE_CONSULTANT + '/api/v1/import/renovation/panorama';
export const API_CMS_LAYOUT_RENOVATION_UPLOAD_GALLERY = API_BASE_CONSULTANT + '/cms/v1/layouts-renovation/upload/gallery';
export const API_CMS_LAYOUT_RENOVATION_GALLERY = API_BASE_CONSULTANT + '/cms/v1/layouts-renovation/gallery';

export const API_CMS_STYLE = API_BASE_CONSULTANT + '/cms/v1/styles';
export const API_CMS_ROOM = API_BASE_CONSULTANT + '/cms/v1/rooms';
export const API_CMS_ROOM_CATEGORY = API_BASE_CONSULTANT + '/cms/v1/resources/room-categories';
export const API_CMS_LOCATION = API_BASE_CONSULTANT + '/cms/v1/resources/locations';