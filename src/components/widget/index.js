import Static from "./static.vue";
import Activities from "./activities.vue";
import Barchart from "./barchart.vue";
import LineChart from "./linechart.vue";
import ActivitiesLayout from "./activitiesLayout.vue";

export default {
  Static,
  Activities,
  Barchart,
  LineChart,
  ActivitiesLayout,
}