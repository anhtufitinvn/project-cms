import { shallowMount } from '@vue/test-utils'
import Home from '@/views/Home.vue'

// import HelloWorld from '@/components/HelloWorld.vue'
// describe('HelloWorld.vue', () => {
//   it('renders props.msg when passed', () => {
//     const msg = 'new message'
//     const wrapper = shallowMount(HelloWorld, {
//       propsData: { msg }
//     })
//     expect(wrapper.text()).toMatch(msg)
//   })
// })

const wrapper = shallowMount(Home, { sync: true })
describe('Home', () => {
  // Inspect the raw component options
  it('has a created hook', () => {
    expect(typeof Home.created).toBe('function')
  })

  // Evaluate the results of functions in
  // the raw component options
  // it('sets the correct default data', () => {
  //   expect(typeof Home.data).toBe('function')
  //   const defaultData = Home.data()
  //   expect(defaultData.message).toBe('hello!')
  // })

  // Inspect the component instance on mount
  // it('correctly sets the message when created', () => {
  //   expect(wrapper.vm.$data.message).toBe('bye!')
  // })

  // Mount an instance and inspect the render output
  it('renders the correct message', () => {
    expect(wrapper.text()).toBe('bye!')
  })
})