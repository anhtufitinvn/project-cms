// vue.config.js
module.exports = {
  runtimeCompiler: true,
  productionSourceMap: false,
  publicPath: process.env.VUE_APP_PUBLIC_PATH || '/'
}