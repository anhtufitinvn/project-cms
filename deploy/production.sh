#!/bin/bash

# abort on errors
set -e

DATE=$(date +"%Y.%m.%d-%H%M")
VERSION="v.${DATE}"
echo "${VERSION}"
echo 'cd to WorkPlace'
# # cd /Users/nguyenhong/Documents/project/ecom-mobile-web
echo 'Done cd to WorkPlace'

echo 'build FE'
npm run build:production
echo 'Done build FE'

echo 'cd to Build'
cd build-production
echo 'Done cd to Unity'

echo 'zip file'
tar cvf "${VERSION}.tar" ./
echo 'zip file'

echo 'cp to Server'
scp -P 2222 "${VERSION}.tar" root@sliveweb2.fitin.link:/data/www/apps.fitin.vn/consultant
scp -P 2222 "${VERSION}.tar" root@sliveweb2.fitin.link:/data/www/apps.fitin.vn/consultant/build.tar
echo 'cp to Server'

echo 'unzip on Server'
ssh -t 'root@sliveweb2.fitin.link' -p 2222 'cd "/tmp" && cd "/data/www/apps.fitin.vn/consultant" && tar xf build.tar ./ && chown -R nginx:nginx ./ && chmod -R 755 ./;'

# content of your script
