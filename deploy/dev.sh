#!/bin/bash

# abort on errors
set -e

DATE=$(date +"%Y.%m.%d-%H%M")
VERSION="v.${DATE}"
echo "${VERSION}"
echo 'cd to WorkPlace'
# # cd /Users/nguyenhong/Documents/project/ecom-mobile-web
echo 'Done cd to WorkPlace'

echo 'build FE'
npm run build:dev
echo 'Done build FE'

echo 'cd to Build'
cd build-dev
echo 'Done cd to Unity'

echo 'zip file'
tar cvf "${VERSION}.tar" ./
echo 'zip file'

echo 'cp to Server'
scp -i ~/.ssh/fitin_dev "${VERSION}.tar" root@45.124.94.203:/var/www/apps.fitin.dev/consultant
scp -i ~/.ssh/fitin_dev "${VERSION}.tar" root@45.124.94.203:/var/www/apps.fitin.dev/consultant/build.tar
echo 'cp to Server'

echo 'unzip on Server'
ssh -t 'root@45.124.94.203' 'cd "/tmp" && cd "/var/www/apps.fitin.dev/consultant" && tar xf build.tar ./ && chown -R nginx:nginx ./ && chmod -R 755 ./;'

# content of your script